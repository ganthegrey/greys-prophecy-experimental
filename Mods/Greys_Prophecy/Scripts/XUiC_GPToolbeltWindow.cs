﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class XUiC_GPToolbeltWindow : XUiC_ToolbeltWindow
{
    private EntityPlayer _localPlayer;
    private CachedStringFormatter<int> _bindingCurrentXp = new CachedStringFormatter<int>((int _i) => _i.ToString());
    private CachedStringFormatter<int> _bindingXpToNextLevel = new CachedStringFormatter<int>((int _i) => _i.ToString());
    private float _lastValue;
    private float _currentValue;
    private float _lastDeficitValue;

    public override void Update(float _dt)
    {
        base.Update(_dt);
        RefreshBindings(false);
    }

    public override void OnOpen()
    {
        base.OnOpen();
        if(_localPlayer == null)
        {
            _localPlayer = xui.playerUI.entityPlayer;
        }

        _currentValue = _lastValue = XUiM_Player.GetLevelPercent(_localPlayer);
    }
    public override bool GetBindingValue(ref string value, BindingItem binding)
    {
        if(binding.FieldName.Equals("xpdisplay", StringComparison.OrdinalIgnoreCase))
        {
            if (_localPlayer != null)
            {
                var xpForNextLevel = _localPlayer.Progression.GetExpForNextLevel();
                var xpToGo = XUiM_Player.GetXPToNextLevel(_localPlayer);
                
                value = _bindingCurrentXp.Format(xpForNextLevel - xpToGo) + " / " + _bindingXpToNextLevel.Format(xpForNextLevel);
            }

            return true;
        }
        return base.GetBindingValue(ref value, binding);
    }
}
