﻿using System;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public class BlockEditableSecureLoot : BlockSecureLoot
{
    private float _takeDelay = 2f;

    private readonly BlockActivationCommand[] _activationCommands = new BlockActivationCommand[]
      {
            new BlockActivationCommand("Search", "search", false),
            new BlockActivationCommand("lock", "lock", false),
            new BlockActivationCommand("unlock", "unlock", false),
            new BlockActivationCommand("keypad", "keypad", false),
            new BlockActivationCommand("take", "hand", false),
            new BlockActivationCommand("edit", "pen", false)
      };

    public BlockEditableSecureLoot()
    {
        HasTileEntity = true;
    }

    public override void OnBlockAdded(WorldBase world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
    {
        if (_blockValue.ischild)
        {
            return;
        }
        if (!(world.GetTileEntity(_chunk.ClrIdx, _blockPos) is TileEntityEditableStorageBox))
        {
            TileEntityEditableStorageBox tileEntitySecureLootContainer = new TileEntityEditableStorageBox(_chunk);
            tileEntitySecureLootContainer.localChunkPos = World.toBlock(_blockPos);
            tileEntitySecureLootContainer.lootListIndex = lootList;
            tileEntitySecureLootContainer.SetContainerSize(LootContainer.lootList[lootList].size, true);
            _chunk.AddTileEntity(tileEntitySecureLootContainer);
        }

        base.OnBlockAdded(world, _chunk, _blockPos, _blockValue);
    }

    public override void Init()
    {
        base.Init();

        _takeDelay = !Properties.Values.ContainsKey("TakeDelay") ? 2f : StringParsers.ParseFloat(Properties.Values["TakeDelay"], 0, -1, NumberStyles.Any);
    }

    public override BlockActivationCommand[] GetBlockActivationCommands(WorldBase _world, BlockValue _blockValue, int _clrIdx, Vector3i _blockPos, EntityAlive _entityFocusing)
    {
        TileEntityEditableStorageBox tileEntitySecureLootContainer = _world.GetTileEntity(_clrIdx, _blockPos) as TileEntityEditableStorageBox;
        if (tileEntitySecureLootContainer == null)
        {
            return new BlockActivationCommand[0];
        }
        string _steamID = GamePrefs.GetString(EnumGamePrefs.PlayerId);
        PersistentPlayerData playerData = _world.GetGameManager().GetPersistentPlayerList().GetPlayerData(tileEntitySecureLootContainer.GetOwner());
        bool flag = !tileEntitySecureLootContainer.IsOwner(_steamID) && (playerData != null && playerData.ACL != null) && playerData.ACL.Contains(_steamID);
        this._activationCommands[0].enabled = true;
        this._activationCommands[1].enabled = (!tileEntitySecureLootContainer.IsLocked() && (tileEntitySecureLootContainer.IsOwner(_steamID) || flag));
        this._activationCommands[2].enabled = (tileEntitySecureLootContainer.IsLocked() && tileEntitySecureLootContainer.IsOwner(_steamID));
        this._activationCommands[3].enabled = ((!tileEntitySecureLootContainer.IsUserAllowed(_steamID) && tileEntitySecureLootContainer.HasPassword() && tileEntitySecureLootContainer.IsLocked()) || tileEntitySecureLootContainer.IsOwner(_steamID));

        _activationCommands[4].enabled = _world.IsMyLandProtectedBlock(_blockPos, _world.GetGameManager().GetPersistentLocalPlayer(), false) && _takeDelay > 0.0;
        _activationCommands[5].enabled = tileEntitySecureLootContainer.IsUserAllowed(_steamID) || _world.IsMyLandProtectedBlock(_blockPos, _world.GetGameManager().GetPersistentLocalPlayer(), false);

        return _activationCommands;
    }

    public override bool OnBlockActivated(int _indexInBlockActivationCommands, WorldBase _world, int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
    {
        if (_blockValue.ischild)
        {
            Vector3i parentPos = Block.list[_blockValue.type].multiBlockPos.GetParentPos(_blockPos, _blockValue);
            BlockValue block = _world.GetBlock(parentPos);
            return this.OnBlockActivated(_indexInBlockActivationCommands, _world, _cIdx, parentPos, block, _player);
        }
        TileEntityEditableStorageBox tileEntitySecureLootContainer = _world.GetTileEntity(_cIdx, _blockPos) as TileEntityEditableStorageBox;
        if (tileEntitySecureLootContainer == null)
        {
            return false;
        }

        EntityPlayerLocal entityPlayerLocal = _player as EntityPlayerLocal;
        LocalPlayerUI uiForPlayer = LocalPlayerUI.GetUIForPlayer(entityPlayerLocal);
        if (_indexInBlockActivationCommands == 4)
        {
            if (!tileEntitySecureLootContainer.IsEmpty())
            {
                GameManager.ShowTooltipWithAlert(entityPlayerLocal, Localization.Get("ttEmptyLootContainerBeforePickup", string.Empty), "ui_denied");
                return false;
            }
            TakeItemWithTimer(_cIdx, _blockPos, _blockValue, _player);
            return true;
        }

        if (_indexInBlockActivationCommands == 5)
        {
            if (uiForPlayer != null)
            {
                ((XUiWindowGroup)uiForPlayer.windowManager.GetWindow("storage_box")).Controller.GetChildByType<XUiC_StorageBoxLabelWindow>().SetTileEntity(tileEntitySecureLootContainer);
                uiForPlayer.windowManager.Open("storage_box", true, false, true);
            }
            return true;
        }

        return base.OnBlockActivated(_indexInBlockActivationCommands, _world, _cIdx, _blockPos, _blockValue, _player);
    }

    public override void OnBlockEntityTransformAfterActivated(WorldBase _world, Vector3i _blockPos, int _cIdx, BlockValue _blockValue, BlockEntityData _ebcd)
    {
        if (_ebcd == null)
        {
            return;
        }
        Chunk chunk = (Chunk)((World)_world).GetChunkFromWorldPos(_blockPos);
        var te = _world.GetTileEntity(_cIdx, _blockPos);

        TileEntityEditableStorageBox tileEditableStorageBox = (TileEntityEditableStorageBox)te;
        if (tileEditableStorageBox == null)
        {
            tileEditableStorageBox = new TileEntityEditableStorageBox(chunk);
            if (tileEditableStorageBox != null)
            {
                tileEditableStorageBox.localChunkPos = World.toBlock(_blockPos);
                chunk.AddTileEntity(tileEditableStorageBox);
            }
        }
        if (tileEditableStorageBox == null)
        {
            Log.Error("Tile Entity Sign was unable to be created!");
            return;
        }
        tileEditableStorageBox.SetBlockEntityData(_ebcd);
        base.OnBlockEntityTransformAfterActivated(_world, _blockPos, _cIdx, _blockValue, _ebcd);
    }

    public override void PlaceBlock(WorldBase _world, BlockPlacement.Result _result, EntityAlive _ea)
    {
        base.PlaceBlock(_world, _result, _ea);
        string playerId = GamePrefs.GetString(EnumGamePrefs.PlayerId);
        TileEntityEditableStorageBox tileEntitySecureLootContainer = _world.GetTileEntity(_result.clrIdx, _result.blockPos) as TileEntityEditableStorageBox;
        if (tileEntitySecureLootContainer != null)
        {
            tileEntitySecureLootContainer.SetEmpty();
            if (_ea != null && _ea.entityType == EntityType.Player)
            {
                tileEntitySecureLootContainer.bPlayerStorage = true;
                tileEntitySecureLootContainer.SetOwner(playerId);
            }
        }
    }

    public void TakeItemWithTimer(int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
    {
        if (_blockValue.damage > 0)
        {
            GameManager.ShowTooltipWithAlert(_player as EntityPlayerLocal, Localization.Get("ttRepairBeforePickup", string.Empty), "ui_denied");
        }
        else
        {
            LocalPlayerUI playerUi = (_player as EntityPlayerLocal).PlayerUI;
            playerUi.windowManager.Open("timer", true, false, true);
            XUiC_Timer childByType = playerUi.xui.GetChildByType<XUiC_Timer>();
            TimerEventData _eventData = new TimerEventData
            {
                Data = new object[4]
                {
                     _cIdx,
                     _blockValue,
                     _blockPos,
                     _player
                }
            };
            _eventData.Event += new TimerEventHandler(this.EventData_Event);
            childByType.SetTimer(_takeDelay, _eventData);
        }
    }

    private void EventData_Event(object obj)
    {
        World world = GameManager.Instance.World;

        var eventData = obj as TimerEventData;

        object[] objArray = (object[])eventData.Data;

        int _clrIdx = (int)objArray[0];
        BlockValue blockValue = (BlockValue)objArray[1];

        Vector3i vector3i = (Vector3i)objArray[2];
        BlockValue block = world.GetBlock(vector3i);

        EntityPlayerLocal entityPlayerLocal = objArray[3] as EntityPlayerLocal;
        if (block.damage > 0)
            GameManager.ShowTooltipWithAlert(entityPlayerLocal, Localization.Get("ttRepairBeforePickup", string.Empty), "ui_denied");
        else if (block.type != blockValue.type)
        {
            GameManager.ShowTooltipWithAlert(entityPlayerLocal, Localization.Get("ttBlockMissingPickup", string.Empty), "ui_denied");
        }
        else
        {
            TileEntitySecureLootContainer tileEntity = world.GetTileEntity(_clrIdx, vector3i) as TileEntitySecureLootContainer;
            if (tileEntity.IsUserAccessing())
            {
                GameManager.ShowTooltipWithAlert(entityPlayerLocal, Localization.Get("ttCantPickupInUse", string.Empty), "ui_denied");
            }
            else
            {
                LocalPlayerUI uiForPlayer = LocalPlayerUI.GetUIForPlayer(entityPlayerLocal);
                ItemStack itemStack = new ItemStack(block.ToItemValue(), 1);
                if (!uiForPlayer.xui.PlayerInventory.AddItem(itemStack, true))
                    uiForPlayer.xui.PlayerInventory.DropItem(itemStack);
                world.SetBlockRPC(_clrIdx, vector3i, BlockValue.Air);
            }
        }
    }
}
