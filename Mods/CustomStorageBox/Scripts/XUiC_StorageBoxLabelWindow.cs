﻿using GUI_2;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class XUiC_StorageBoxLabelWindow : XUiController
{
    private XUiC_TextInput _textInput;
    private BlockEntityData _blockEntityData;
    TileEntityEditableStorageBox _tileEntity;
    private string _signText;

    public string SignText
    {
        get
        {
            return _signText;
        }
        set
        {
            _signText = value;
            if (_tileEntity != null)
            {
                _tileEntity.SetText(_signText, true);
            }
        }
    }

    public override void Init()
    {
        base.Init();
        XUiView xuiView = (XUiV_Button)GetChildById("clickable").ViewComponent;
        _textInput = GetChildByType<XUiC_TextInput>();
        _textInput.OnChangeHandler += TextInput_OnChangeHandler;
        _textInput.OnSubmitHandler += TextInput_OnSubmitHandler;
        _textInput.OnInputAbortedHandler += TextInput_OnInputAbortedHandler;
        xuiView.Controller.OnPress += closeButton_OnPress;
    }

    public override void OnOpen()
    {
        base.OnOpen();
        SignText = _tileEntity.GetText();
        _textInput.OnChangeHandler -= TextInput_OnChangeHandler;
        _textInput.Text = SignText;
        _textInput.OnChangeHandler += TextInput_OnChangeHandler;
        _textInput.SelectOrVirtualKeyboard();
        xui.playerUI.entityPlayer.PlayOneShot("open_sign", false);
    }

    public override void OnClose()
    {
        _tileEntity.SetModified();
        _tileEntity.SetUserAccessing(false);
        GameManager.Instance.TEUnlockServer(_tileEntity.GetChunk().ClrIdx, _tileEntity.ToWorldPos(), _tileEntity.entityId);
        base.OnClose();
        xui.playerUI.entityPlayer.PlayOneShot("close_sign", false);
    }

    public void SetTileEntity(TileEntityEditableStorageBox tileEntity)
    {
        _tileEntity = tileEntity;
    }

    private void TextInput_OnInputAbortedHandler(XUiController _sender)
    {
        xui.playerUI.windowManager.Close(WindowGroup.ID);
    }

    private void TextInput_OnChangeHandler(XUiController _sender, string _text, bool _changeFromCode)
    {
        SignText = _text;
    }

    private void TextInput_OnSubmitHandler(XUiController _sender, string _text)
    {
        SignText = _text;
        xui.playerUI.windowManager.Close(WindowGroup.ID);
    }

    private void closeButton_OnPress(XUiController _sender, OnPressEventArgs _e)
    {
        xui.playerUI.windowManager.Close(WindowGroup.ID);
    }
}
