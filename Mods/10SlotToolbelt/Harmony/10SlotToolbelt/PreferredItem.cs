using DMT;
using Harmony;
using InControl;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using UnityEngine;

class Khaine_PreferredItem_Patch
{
    public class Khaine_PreferredItem_Logger
    {
        public static bool blDisplayLog = true;

        public static void Log(String strMessage)
        {
            if (blDisplayLog)
                UnityEngine.Debug.Log(strMessage);
        }
    }
/*
    public class Khaine_PreferredItem_Init : IHarmony
    {
        public void Start()
        {
            Khaine_PreferredItem_Logger.Log(" Loading Patch: " + GetType().ToString());
            var harmony = HarmonyInstance.Create(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    } */
	
    //[HarmonyPatch(typeof(Inventory))]
	[HarmonyPatch(typeof(Inventory), MethodType.Constructor)]
    [HarmonyPatch("Inventory")]
	[HarmonyPatch(new Type[] { typeof(IGameManager), typeof(EntityAlive) })]
    public class Khaine_InventorypreferredItemSlots
    {
        // Loops around the instructions and removes the return condition.
        static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
        {
			Khaine_PreferredItem_Logger.Log("Patching preferredItemSlots()");
			
            int counter = 0;

            // Grab all the instructions
            var codes = new List<CodeInstruction>(instructions);
            for (int i = 5; i < codes.Count; i++)
            {
                if (codes[i].opcode == OpCodes.Ldc_I4_8)
                {
                    counter++;
                    Khaine_PreferredItem_Logger.Log("Adjusting 8 to 10");

                    codes[i].opcode = OpCodes.Ldc_I4; // convert to the right thing
                    codes[i].operand = 10;
                    Khaine_PreferredItem_Logger.Log("Done with 8 to 10");
                }

            }
			Khaine_PreferredItem_Logger.Log("Done With Patching preferredItemSlots()");
			
            return codes.AsEnumerable();
        }		
    }
}